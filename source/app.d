import std.stdio;
import std.math;
import std.conv;
import std.format;
import std.string;
import derelict.sdl2.sdl;
import derelict.sdl2.image;
import derelict.sdl2.mixer;
import derelict.sdl2.ttf;
import derelict.sdl2.net;

immutable uint texWidth  = 64;
immutable uint texHeight = 64;

immutable uint mapWidth = 24;
immutable uint mapHeight = 24;

immutable uint[mapHeight][mapWidth] worldMap=
[
  [4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,7,7,7,7,7,7,7,7],
  [4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,7],
  [4,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
  [4,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
  [4,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,7],
  [4,0,4,0,0,0,0,5,5,5,5,5,5,5,5,5,7,7,0,7,7,7,7,7],
  [4,0,5,0,0,0,0,5,0,5,0,5,0,5,0,5,7,0,0,0,7,7,7,1],
  [4,0,6,0,0,0,0,5,0,0,0,0,0,0,0,5,7,0,0,0,0,0,0,8],
  [4,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,1],
  [4,0,8,0,0,0,0,5,0,0,0,0,0,0,0,5,7,0,0,0,0,0,0,8],
  [4,0,0,0,0,0,0,5,0,0,0,0,0,0,0,5,7,0,0,0,7,7,7,1],
  [4,0,0,0,0,0,0,5,5,5,5,0,5,5,5,5,7,7,7,7,7,7,7,1],
  [6,6,6,6,6,6,6,6,6,6,6,0,6,6,6,6,6,6,6,6,6,6,6,6],
  [8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4],
  [6,6,6,6,6,6,0,6,6,6,6,0,6,6,6,6,6,6,6,6,6,6,6,6],
  [4,4,4,4,4,4,0,4,4,4,6,0,6,2,2,2,2,2,2,2,3,3,3,3],
  [4,0,0,0,0,0,0,0,0,4,6,0,6,2,0,0,0,0,0,2,0,0,0,2],
  [4,0,0,0,0,0,0,0,0,0,0,0,6,2,0,0,5,0,0,2,0,0,0,2],
  [4,0,0,0,0,0,0,0,0,4,6,0,6,2,0,0,0,0,0,2,2,0,2,2],
  [4,0,6,0,6,0,0,0,0,4,6,0,0,0,0,0,5,0,0,0,0,0,0,2],
  [4,0,0,5,0,0,0,0,0,4,6,0,6,2,0,0,0,0,0,2,2,0,2,2],
  [4,0,6,0,6,0,0,0,0,4,6,0,6,2,0,0,5,0,0,2,0,0,0,2],
  [4,0,0,0,0,0,0,0,0,4,6,0,6,2,0,0,0,0,0,2,0,0,0,2],
  [4,4,4,4,4,4,4,4,4,4,1,1,1,2,2,2,2,2,2,3,3,3,3,3]
];

double posX = 5, posY = 5;  //x and y start position
double dirX = -1, dirY = 0; //initial direction vector
double planeX = 0, planeY = 0.66; //the 2d raycaster version of camera plane

immutable SDL_Color RGB_Red    = {250, 25, 25,255};
immutable SDL_Color RGB_Green  = { 25,250, 25,255};
immutable SDL_Color RGB_Blue   = { 25, 25,250,255};
immutable SDL_Color RGB_White  = {250,250,250,255};
immutable SDL_Color RGB_Yellow = {250,250, 25,255};
immutable SDL_Color RGB_Black  = {  0,  0,  0,255};

TTF_Font* debug_font = null;

immutable uint NUM_TEXTURES = 8;
SDL_Surface*[NUM_TEXTURES] Surfaces;
SDL_Texture*[NUM_TEXTURES] Textures;
string[NUM_TEXTURES] TextureFilenames =
[
  "pics/eagle.png",
  "pics/redbrick.png",
  "pics/purplestone.png",
  "pics/greystone.png",
  "pics/bluestone.png",
  "pics/mossy.png",
  "pics/wood.png",
  "pics/colorstone.png"
];

void DebugDisplayTextures(SDL_Renderer* renderer, uint deltaTime_milliseconds)
{
    int frameWidth, frameHeight;
    if (SDL_GetRendererOutputSize(renderer, &frameWidth, &frameHeight) != 0)
    {
        writeln( "Don't know what size the frame should be! SDL_Error: ", SDL_GetError() );
        return;
    }

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);

    SDL_Rect destRect;
    destRect.h = texHeight;
    destRect.w = texWidth;
    destRect.x = texWidth;
    destRect.y = 64;
    for (uint i = 0; i < NUM_TEXTURES; ++i)
    {
        SDL_RenderCopy(renderer, Textures[i], null, &destRect);
        destRect.x += texWidth;
    }

    SDL_RenderPresent(renderer);
}

Uint32 getpixel(SDL_Surface* surface, uint x, uint y)
{
    int bpp = surface.format.BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = cast(Uint8*)surface.pixels + y * surface.pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        //break;

    case 2:
        return *cast(Uint16*)p;
        //break;

    case 3:
        //if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
        //    return p[0] << 16 | p[1] << 8 | p[2];
        //else
            return p[0] | p[1] << 8 | p[2] << 16;
        //break;

    case 4:
        return *cast(Uint32*)p;
        //break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

void RenderFrame(SDL_Renderer* renderer, uint deltaTime_milliseconds)
{
    int frameWidth, frameHeight;
    if (SDL_GetRendererOutputSize(renderer, &frameWidth, &frameHeight) != 0)
    {
        writeln( "Don't know what size the frame should be! SDL_Error: ", SDL_GetError() );
        return;
    }

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);

    for(int x = 0; x < frameWidth; x++)
    {
      //calculate ray position and direction
      double cameraX = 2 * x / double(frameWidth) - 1; //x-coordinate in camera space
      double rayDirX = dirX + planeX * cameraX;
      double rayDirY = dirY + planeY * cameraX;
      //which box of the map we're in
      int mapX = to!int(posX);//int(posX);
      int mapY = to!int(posY);

      //length of ray from current position to next x or y-side
      double sideDistX;
      double sideDistY;

       //length of ray from one x or y-side to next x or y-side
      double deltaDistX = std.math.abs(1 / rayDirX);
      double deltaDistY = std.math.abs(1 / rayDirY);
      double perpWallDist;

      //what direction to step in x or y-direction (either +1 or -1)
      int stepX;
      int stepY;

      int hit = 0; //was there a wall hit?
      int side; //was a NS or a EW wall hit?
      //calculate step and initial sideDist
      if (rayDirX < 0)
      {
        stepX = -1;
        sideDistX = (posX - mapX) * deltaDistX;
      }
      else
      {
        stepX = 1;
        sideDistX = (mapX + 1.0 - posX) * deltaDistX;
      }
      if (rayDirY < 0)
      {
        stepY = -1;
        sideDistY = (posY - mapY) * deltaDistY;
      }
      else
      {
        stepY = 1;
        sideDistY = (mapY + 1.0 - posY) * deltaDistY;
      }
      //perform DDA
      while (hit == 0)
      {
        //jump to next map square, OR in x-direction, OR in y-direction
        if (sideDistX < sideDistY)
        {
          sideDistX += deltaDistX;
          mapX += stepX;
          side = 0;
        }
        else
        {
          sideDistY += deltaDistY;
          mapY += stepY;
          side = 1;
        }
        //Check if ray has hit a wall
        if (worldMap[mapX][mapY] > 0) hit = 1;
      }
      //Calculate distance projected on camera direction (Euclidean distance will give fisheye effect!)
      if (side == 0) perpWallDist = (mapX - posX + (1 - stepX) / 2) / rayDirX;
      else           perpWallDist = (mapY - posY + (1 - stepY) / 2) / rayDirY;

      //Calculate height of line to draw on screen
      int lineHeight = cast(int)(frameHeight / perpWallDist);

      //calculate lowest and highest pixel to fill in current stripe
      int drawStart = -lineHeight / 2 + frameHeight / 2;
      if(drawStart < 0)drawStart = 0;
      int drawEnd = lineHeight / 2 + frameHeight / 2;
      if(drawEnd >= frameHeight)drawEnd = frameHeight - 1;

      //texturing calculations
      int texNum = worldMap[mapX][mapY] - 1; //1 subtracted from it so that texture 0 can be used!

      //calculate value of wallX
      double wallX; //where exactly the wall was hit
      if (side == 0) wallX = posY + perpWallDist * rayDirY;
      else           wallX = posX + perpWallDist * rayDirX;
      wallX -= floor((wallX));

      //x coordinate on the texture
      int texX = to!int(wallX * double(texWidth));
      if(side == 0 && rayDirX > 0) texX = texWidth - texX - 1;
      if(side == 1 && rayDirY < 0) texX = texWidth - texX - 1;

      //draw the pixels of the stripe as a vertical line
      //texNum = 0;
      SDL_PixelFormat* fmt = Surfaces[texNum].format;
      SDL_LockSurface(Surfaces[texNum]);
      Uint32 pixel;
      SDL_Color color;
      for(int y = drawStart; y<drawEnd; y++)
      {
        uint d = y * 256 - frameHeight * 128 + lineHeight * 128;  //256 and 128 factors to avoid floats
        // TODO: avoid the division to speed this up

        uint texY = ((d * texHeight) / lineHeight) / 256;
        pixel = getpixel(Surfaces[texNum], texX, texY);
        //make color darker for y-sides: R, G and B byte each divided through two with a "shift" and an "and"
        if(side == 1) pixel = (pixel >> 1) & 8355711;

        SDL_GetRGB(pixel, fmt, &color.r, &color.g, &color.b);

        SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, SDL_ALPHA_OPAQUE);
        SDL_RenderDrawPoint(renderer, x, y);
      }
      SDL_UnlockSurface(Surfaces[texNum]);
    }

    // FPS Display
    //string fpsString = format("dT: %u", deltaTime_milliseconds);
    string fpsString = format("FPS: %3.1f",1.0 / (deltaTime_milliseconds / 1000.0));
    SDL_Surface* fpsSurface = TTF_RenderText_Shaded(debug_font, std.string.toStringz(fpsString), RGB_White, RGB_Black);
    SDL_Rect fpsSize = { 5, 5, fpsSurface.w, fpsSurface.h };
    SDL_Texture* fpsTexture = SDL_CreateTextureFromSurface(renderer,fpsSurface);
    SDL_FreeSurface(fpsSurface);

    SDL_RenderCopy(renderer, fpsTexture, null, &fpsSize);
    SDL_RenderPresent(renderer);

    SDL_DestroyTexture(fpsTexture);
}

void HandleKeypress(SDL_Keycode key, const double moveSpeed, const double rotSpeed)
{
    int tileX, tileY;
    switch(key)
    {
        case SDLK_UP:
            tileX = to!int(posX + dirX * moveSpeed);
            if (tileX >= mapWidth) tileX = mapWidth - 1;
            tileY = to!int(posY);
            if(worldMap[tileX][tileY] == false) posX += dirX * moveSpeed;

            tileX = to!int(posX);
            tileY = to!int(posY + dirY * moveSpeed);
            if (tileY >= mapHeight) tileY = mapHeight - 1;
            if(worldMap[tileX][tileY] == false) posY += dirY * moveSpeed;
            break;

        case SDLK_DOWN:
            tileX = to!int(posX - dirX * moveSpeed);
            if (tileX < 0) tileX = 0;
            tileY = to!int(posY);
            if(worldMap[tileX][tileY] == false) posX -= dirX * moveSpeed;

            tileX = to!int(posX);
            tileY = to!int(posY - dirY * moveSpeed);
            if (tileY < 0) tileY = 0;
            if(worldMap[tileX][tileY] == false) posY -= dirY * moveSpeed;
            break;

        //rotate to the right
        case SDLK_RIGHT:
            //both camera direction and camera plane must be rotated
            double oldDirX = dirX;
            dirX = dirX * cos(-rotSpeed) - dirY * sin(-rotSpeed);
            dirY = oldDirX * sin(-rotSpeed) + dirY * cos(-rotSpeed);
            double oldPlaneX = planeX;
            planeX = planeX * cos(-rotSpeed) - planeY * sin(-rotSpeed);
            planeY = oldPlaneX * sin(-rotSpeed) + planeY * cos(-rotSpeed);
            break;

        //rotate to the left
        case SDLK_LEFT:
            //both camera direction and camera plane must be rotated
            double oldDirX = dirX;
            dirX = dirX * cos(rotSpeed) - dirY * sin(rotSpeed);
            dirY = oldDirX * sin(rotSpeed) + dirY * cos(rotSpeed);
            double oldPlaneX = planeX;
            planeX = planeX * cos(rotSpeed) - planeY * sin(rotSpeed);
            planeY = oldPlaneX * sin(rotSpeed) + planeY * cos(rotSpeed);
            break;

        default:
            // ignore keypresses that are not needed
            break;
    }
}

bool ProcessInputs(uint deltaTime_milliseconds)
{
    //speed modifiers
    immutable double moveSpeed = deltaTime_milliseconds * 0.05; //the constant value is in squares/second
    immutable double rotSpeed = deltaTime_milliseconds * 0.02; //the constant value is in radians/second

    SDL_Event event;

    while(SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            case SDL_QUIT:
                return true;

            case SDL_KEYDOWN:
                HandleKeypress(event.key.keysym.sym, moveSpeed, rotSpeed);
                break;

            default:
                writeln("event: ", event.type);
        }
    }

    return false;
}

void EventLoop(SDL_Renderer* renderer)
{
    uint nowTime = SDL_GetTicks();
    uint oldTime;
    uint dt;
    bool done = false;

    while (!done)
    {
        oldTime = nowTime;
        nowTime = SDL_GetTicks();
        dt = nowTime - oldTime;

        done = ProcessInputs(dt);
        RenderFrame(renderer, dt);
        //DebugDisplayTextures(renderer, dt);

        SDL_Delay(1);
    }
}

SDL_Renderer* InitializeGraphics()
{
    immutable int SCREEN_WIDTH = 640;
    immutable int SCREEN_HEIGHT = 480;

    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0)
    {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return null;
    }
    else
    {
        SDL_Window* window = SDL_CreateWindow( "SDL Tutorial",
                                               SDL_WINDOWPOS_UNDEFINED,
                                               SDL_WINDOWPOS_UNDEFINED,
                                               SCREEN_WIDTH, SCREEN_HEIGHT,
                                               SDL_WINDOW_SHOWN );
        if( window == null )
        {
            printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
            return null;
        }
        else
        {
            if (TTF_Init() < 0)
            {
                printf( "TTF Library could not be initialized" );
                return null;
            }

            if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
            {
                printf( "Image library could not initialize PNG support" );
                return null;
            }
            return SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
        }
    }
}

// returns: true  == textures loaded successfully
//          false == failed to load the textures
bool LoadTextures(SDL_Renderer* renderer)
{
    bool loadedOK = true;

    for( uint i = 0; i < NUM_TEXTURES; ++i )
    {
        Surfaces[i]=IMG_Load(std.string.toStringz(TextureFilenames[i]));
        if(Surfaces[i] == null)
        {
            Textures[i] = null;
            printf("IMG_Load: %s\n", IMG_GetError());
            loadedOK = false;
        }
        else
        {
            Textures[i] = SDL_CreateTextureFromSurface(renderer, Surfaces[i]);
        }
    }
    return loadedOK;
}

void DestroyTextures()
{
    for( uint i = 0; i < NUM_TEXTURES; ++i )
    {
        SDL_DestroyTexture(Textures[i]);
        SDL_FreeSurface(Surfaces[i]);
    }
}

void main()
{
    // Load the SDL 2 library.
    DerelictSDL2.load();

    // Load the SDL2_image library.
    DerelictSDL2Image.load();

    // Load the SDL2_mixer library.
    DerelictSDL2Mixer.load();

    // Load the SDL2_ttf library
    DerelictSDL2ttf.load();

    // Load the SDL2_net library.
    DerelictSDL2Net.load();

    SDL_Renderer* renderer = InitializeGraphics();
    debug_font = TTF_OpenFont("debug_gui_font.ttf", 16);
    if (renderer != null && debug_font != null)
    {
        if (LoadTextures(renderer))
        {
            EventLoop(renderer);
            DestroyTextures();
        }
        TTF_CloseFont(debug_font);
    }

    IMG_Quit();
    TTF_Quit();
    SDL_Quit();

    return;
}
